#!/usr/bin/env bash
#SBATCH --job-name=mpnn20
#SBATCH --output=mpnn20ckpt%j.log
#SBATCH --error=mpnn20ckpt%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash


DEVICES="0"

#WANDB_MODE=dryrun
wandb login 9815f3b0e8ce08b8878d8ca9a84bca3dd70f3978
wandb offline

CUDA_VISIBLE_DEVICES="$DEVICES" python mpnn20_ckpt.py



















